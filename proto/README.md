# Testing the Protocol Buffer API

All of commands here assumes that you're in the current directory - `proto`.

## Requirements

To test the protobuf API in Achilles you'll need `curl` and `protoc`.

## LeaguesSeasons

To test getting a list of all available pairs of leagues and seasons:

    curl -XPOST --data-binary "" http://localhost:4000/api/leagues_seasons.proto | protoc --decode achilles.proto.LeaguesSeasons ./match.proto

## Matches

To test getting a list of all matches for a pair of league and season:

    cat proto.msg | protoc --encode achilles.proto.LeaguesSeason ./match.proto | curl -XPOST --data-binary @- http://localhost:4000/api/matches.proto | protoc --decode achilles.proto.Matches ./match.proto

# Regenerating the elixir code

    protoc --elixir_out=../lib/achilles/proto match.proto

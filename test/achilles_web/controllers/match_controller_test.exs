defmodule AchillesWeb.MatchControllerTest do
  use AchillesWeb.ConnCase

  alias Achilles.Matches

  @create_attrs %{
    league: "some league",
    season: "some season",
    date: ~D[2010-04-17],
    away_team: "some away_team",
    home_team: "some home_team",
    hthg: 42,
    htag: 42,
    htr: "D",
    fthg: 42,
    ftag: 42,
    ftr: "D"
  }

  def fixture(:match) do
    {:ok, match} = Matches.create_match(@create_attrs)
    match
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "leagues_seasons_index" do
    test "lists all leagues and seasons pairs", %{conn: conn} do
      conn = get(conn, match_path(conn, :leagues_seasons_index))
      assert json_response(conn, 200)["data"] == []
      match = fixture(:match)
      conn = get(conn, match_path(conn, :leagues_seasons_index))

      assert json_response(conn, 200)["data"] == [
               %{"league" => match.league, "season" => match.season}
             ]
    end
  end

  describe "leagues_seasons_matches" do
    test "lists all results for league and season pair", %{conn: conn} do
      conn = get(conn, match_path(conn, :leagues_seasons_matches, league: "test", season: "data"))

      assert json_response(conn, 200)["data"] == []
      match = fixture(:match)

      conn =
        get(
          conn,
          match_path(conn, :leagues_seasons_matches, league: match.league, season: match.season)
        )

      assert json_response(conn, 200)["data"] == [
               %{
                 "id" => match.id,
                 "league" => match.league,
                 "season" => match.season,
                 "date" => to_string(match.date),
                 "home_team" => match.home_team,
                 "away_team" => match.away_team,
                 "hthg" => match.hthg,
                 "htag" => match.htag,
                 "htr" => match.htr,
                 "fthg" => match.fthg,
                 "ftag" => match.ftag,
                 "ftr" => match.ftr
               }
             ]
    end
  end
end

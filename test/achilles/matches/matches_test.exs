defmodule Achilles.MatchesTest do
  use Achilles.DataCase

  alias Achilles.Matches

  describe "matches" do
    alias Achilles.Matches.Match

    @valid_attrs %{
      away_team: "some away_team",
      date: "2010-04-17",
      ftag: 42,
      fthg: 42,
      ftr: "some ftr",
      home_team: "some home_team",
      htag: 42,
      hthg: 42,
      htr: "some htr",
      league: "some league",
      season: "some season"
    }
    @update_attrs %{
      away_team: "some updated away_team",
      date: "2011-05-18",
      ftag: 43,
      fthg: 43,
      ftr: "some updated ftr",
      home_team: "some updated home_team",
      htag: 43,
      hthg: 43,
      htr: "some updated htr",
      league: "some updated league",
      season: "some updated season"
    }
    @invalid_attrs %{
      away_team: nil,
      date: nil,
      ftag: nil,
      fthg: nil,
      ftr: nil,
      home_team: nil,
      htag: nil,
      hthg: nil,
      htr: nil,
      league: nil,
      season: nil
    }

    def match_fixture(attrs \\ %{}) do
      {:ok, match} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Matches.create_match()

      match
    end

    test "list_matches/0 returns all matches" do
      match = match_fixture()
      assert Matches.list_matches() == [match]
    end

    test "list_leagues_seasons/0 returns all the league and season pairs for which there are results available" do
      match = match_fixture()
      assert Matches.list_leagues_seasons() == [%{league: match.league, season: match.season}]
    end

    test "list_matches_for_leagues_seasons/2 returns all the results for a specific league and season pair" do
      match = match_fixture()
      assert Matches.list_matches_for_leagues_seasons!(match.league, match.season) == [match]
      assert Matches.list_matches_for_leagues_seasons!("no such", "pairs") == []
    end

    test "get_match!/1 returns the match with given id" do
      match = match_fixture()
      assert Matches.get_match!(match.id) == match
    end

    test "create_match/1 with valid data creates a match" do
      assert {:ok, %Match{} = match} = Matches.create_match(@valid_attrs)
      assert match.away_team == "some away_team"
      assert match.date == ~D[2010-04-17]
      assert match.ftag == 42
      assert match.fthg == 42
      assert match.ftr == "some ftr"
      assert match.home_team == "some home_team"
      assert match.htag == 42
      assert match.hthg == 42
      assert match.htr == "some htr"
      assert match.league == "some league"
      assert match.season == "some season"
    end

    test "create_match/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Matches.create_match(@invalid_attrs)
    end

    test "update_match/2 with valid data updates the match" do
      match = match_fixture()
      assert {:ok, match} = Matches.update_match(match, @update_attrs)
      assert %Match{} = match
      assert match.away_team == "some updated away_team"
      assert match.date == ~D[2011-05-18]
      assert match.ftag == 43
      assert match.fthg == 43
      assert match.ftr == "some updated ftr"
      assert match.home_team == "some updated home_team"
      assert match.htag == 43
      assert match.hthg == 43
      assert match.htr == "some updated htr"
      assert match.league == "some updated league"
      assert match.season == "some updated season"
    end

    test "update_match/2 with invalid data returns error changeset" do
      match = match_fixture()
      assert {:error, %Ecto.Changeset{}} = Matches.update_match(match, @invalid_attrs)
      assert match == Matches.get_match!(match.id)
    end

    test "delete_match/1 deletes the match" do
      match = match_fixture()
      assert {:ok, %Match{}} = Matches.delete_match(match)
      assert_raise Ecto.NoResultsError, fn -> Matches.get_match!(match.id) end
    end

    test "change_match/1 returns a match changeset" do
      match = match_fixture()
      assert %Ecto.Changeset{} = Matches.change_match(match)
    end
  end
end

# API

Achilles provides its users with two kinds of HTTP APIs: JSON and Protocol Buffers.

Both have the same capabilities and there are only two differences:

* All Protocol Buffer requests are made by sending POST to the server while JSON API uses only GETs.
* Routes for the requests have different extension(.proto and .json)

## JSON API

### [GET] List LeaguesSeasons

This request uses `http://achilles:4000/api/leagues_seasons.json` URL.

Format of the response:

```json
{
    "data": [
        {"season":"2018-2019", "league":"Example League"},
        ...
    ]
}
```

### [GET] List Matches in LeaguesSeason

This request requires an user to pass two params in the URL:
`http://achilles:4000/api/matches.json?league=Example%20League&season=2018-2019`.

Format of the response:

```json
{
    "data": [
        {
            "id": 42,
            "league": "Example League",
            "season": "2018-2019",
            "date": "2018-09-22",
            "home_team": "Adams",
            "away_team": "Bobs",
            "hthg": 0, "htag": 1, "htr": "A",
            "fthg": 3, "ftag": 2, "ftr": "H"
        },
        ...
    ]
}
```

## Protocol Buffers API

Detailed definition of this API can be found in [match.proto file](proto/match.proto).

### [POST] List LeaguesSeasons

This request uses `http://achilles:4000/api/leagues_seasons.proto` URL and expects user to send
there empty message.

Data returned in response are the same as in JSON API, but packet to the binary form.

Instructions on unpacking this stream can be found in [the proto dir](proto/README.md).

Format of the response after printing:

```
leagues_season {
  league: "Example League"
  season: "2018-2019"
}
...
```

### [POST] List Matches in LeaguesSeason

This request uses `http://achilles:4000/api/matches.proto` URL and expects user to send
there LeaguesSeason message.

Example of such unencoded message is available in
[proto.msg file](proto/proto.msg).

Data returned in response are the same as in JSON API, but packet to the binary form.

Instructions on unpacking this stream can be found in [the proto dir](proto/README.md).

Format of the response after printing:

```
match {
  id: 42
  league: "Example League"
  season: "2018-2019"
  date {
    year: 2018
    month: 9
    day: 22
  }
  home_team: "Adams"
  away_team: "Bobs"
  htag: 1
  htr: "A"
  fthg: 3
  ftag: 2
  ftr: "H"
}
...
```

It's important to notice that in the Protocol Buffer version, if the value
of one of the params is equal to the default value of this param - it's not sent
over. Eg. in the example above there's `hthg` missing, because at half time in
this match `Adams` team had no goals.

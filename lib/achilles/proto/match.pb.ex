defmodule Achilles.Proto.Date do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          year: integer,
          month: integer,
          day: integer
        }
  defstruct [:year, :month, :day]

  field :year, 1, type: :int32
  field :month, 2, type: :int32
  field :day, 3, type: :int32
end

defmodule Achilles.Proto.Match do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: integer,
          league: String.t(),
          season: String.t(),
          date: Achilles.Proto.Date.t(),
          home_team: String.t(),
          away_team: String.t(),
          fthg: non_neg_integer,
          ftag: non_neg_integer,
          ftr: String.t(),
          hthg: non_neg_integer,
          htag: non_neg_integer,
          htr: String.t()
        }
  defstruct [
    :id,
    :league,
    :season,
    :date,
    :home_team,
    :away_team,
    :fthg,
    :ftag,
    :ftr,
    :hthg,
    :htag,
    :htr
  ]

  field :id, 1, type: :int64
  field :league, 2, type: :string
  field :season, 3, type: :string
  field :date, 4, type: Achilles.Proto.Date
  field :home_team, 5, type: :string
  field :away_team, 6, type: :string
  field :fthg, 7, type: :uint32
  field :ftag, 8, type: :uint32
  field :ftr, 9, type: :string
  field :hthg, 10, type: :uint32
  field :htag, 11, type: :uint32
  field :htr, 12, type: :string
end

defmodule Achilles.Proto.Matches do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          match: [Achilles.Proto.Match.t()]
        }
  defstruct [:match]

  field :match, 1, repeated: true, type: Achilles.Proto.Match
end

defmodule Achilles.Proto.LeaguesSeason do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          league: String.t(),
          season: String.t()
        }
  defstruct [:league, :season]

  field :league, 1, type: :string
  field :season, 2, type: :string
end

defmodule Achilles.Proto.LeaguesSeasons do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          leagues_season: [Achilles.Proto.LeaguesSeason.t()]
        }
  defstruct [:leagues_season]

  field :leagues_season, 1, repeated: true, type: Achilles.Proto.LeaguesSeason
end

defmodule Achilles.Proto.EmptyParams do
  @moduledoc false
  use Protobuf, syntax: :proto3

  defstruct []
end

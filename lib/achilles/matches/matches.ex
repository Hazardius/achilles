defmodule Achilles.Matches do
  @moduledoc """
  The Matches context.
  """

  import Ecto.Query, warn: false
  alias Achilles.Repo

  alias Achilles.Matches.Match

  @doc """
  Returns the list of matches.

  ## Examples

      iex> list_matches()
      [%Match{}, ...]

  """
  def list_matches do
    Repo.all(Match)
  end

  @doc """
  Returns the list of all the league and season pairs for which there are results available.
  """
  def list_leagues_seasons do
    Match
    |> select([m], %{:league => m.league, :season => m.season})
    |> distinct(true)
    |> Repo.all()
  end

  @doc """
  Translates a single league and season pair to it's Protocol Buffer structure.
  """
  def to_pb_msg_ls(leagues_season) do
    Achilles.Proto.LeaguesSeason.new(
      league: leagues_season.league,
      season: leagues_season.season
    )
  end

  @doc """
  Creates a single Achilles.Proto.Matches structure from a list of matches Protocol Buffer structures.
  """
  def to_pb_msg_m(matches) do
    Achilles.Proto.Matches.new(match: Enum.map(matches, &Match.to_pb_msg/1))
  end

  @doc """
  Returns the list of all the league and season pairs for which there are results available.
  """
  def list_matches_for_leagues_seasons!(league, season) do
    Match
    |> where([m], m.league == ^league and m.season == ^season)
    |> Repo.all()
  end

  @doc """
  Gets a single match.

  Raises `Ecto.NoResultsError` if the Match does not exist.

  ## Examples

      iex> get_match!(123)
      %Match{}

      iex> get_match!(456)
      ** (Ecto.NoResultsError)

  """
  def get_match!(id), do: Repo.get!(Match, id)

  @doc """
  Creates a match.

  ## Examples

      iex> create_match(%{field: value})
      {:ok, %Match{}}

      iex> create_match(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_match(attrs \\ %{}) do
    %Match{}
    |> Match.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a match.

  ## Examples

      iex> update_match(match, %{field: new_value})
      {:ok, %Match{}}

      iex> update_match(match, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_match(%Match{} = match, attrs) do
    match
    |> Match.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Match.

  ## Examples

      iex> delete_match(match)
      {:ok, %Match{}}

      iex> delete_match(match)
      {:error, %Ecto.Changeset{}}

  """
  def delete_match(%Match{} = match) do
    Repo.delete(match)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking match changes.

  ## Examples

      iex> change_match(match)
      %Ecto.Changeset{source: %Match{}}

  """
  def change_match(%Match{} = match) do
    Match.changeset(match, %{})
  end
end

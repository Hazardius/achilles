defmodule Achilles.Matches.Match do
  use Ecto.Schema
  import Ecto.Changeset

  schema "matches" do
    field(:away_team, :string)
    field(:date, :date)
    field(:ftag, :integer)
    field(:fthg, :integer)
    field(:ftr, :string)
    field(:home_team, :string)
    field(:htag, :integer)
    field(:hthg, :integer)
    field(:htr, :string)
    field(:league, :string)
    field(:season, :string)

    timestamps()
  end

  @doc false
  def changeset(match, attrs) do
    match
    |> cast(attrs, [
      :league,
      :season,
      :date,
      :home_team,
      :away_team,
      :fthg,
      :ftag,
      :ftr,
      :hthg,
      :htag,
      :htr
    ])
    |> validate_required([
      :league,
      :season,
      :date,
      :home_team,
      :away_team,
      :fthg,
      :ftag,
      :ftr,
      :hthg,
      :htag,
      :htr
    ])
  end

  @doc """
  Translates a single match to it's Protocol Buffer structure.
  """
  def to_pb_msg(match) do
    {year, month, day} = Date.to_erl(match.date)

    Achilles.Proto.Match.new(
      id: match.id,
      league: match.league,
      season: match.season,
      date:
        Achilles.Proto.Date.new(
          year: year,
          month: month,
          day: day
        ),
      home_team: match.home_team,
      away_team: match.away_team,
      fthg: match.fthg,
      ftag: match.ftag,
      ftr: match.ftr,
      hthg: match.hthg,
      htag: match.htag,
      htr: match.htr
    )
  end
end

defmodule AchillesWeb.Router do
  use AchillesWeb, :router

  pipeline :api do
    plug(:accepts, ["json", "proto"])
  end

  scope "/api", AchillesWeb do
    pipe_through(:api)

    get("/leagues_seasons.json", MatchController, :leagues_seasons_index)
    get("/matches.json", MatchController, :leagues_seasons_matches)
    post("/leagues_seasons.proto", MatchController, :leagues_seasons_index_proto)
    post("/matches.proto", MatchController, :leagues_seasons_matches_proto)
  end
end

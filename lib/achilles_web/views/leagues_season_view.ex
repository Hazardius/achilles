defmodule AchillesWeb.LeaguesSeasonView do
  use AchillesWeb, :view
  alias AchillesWeb.LeaguesSeasonView

  def render("index.proto", %{leagues_seasons: leagues_seasons}) do
    Achilles.Proto.LeaguesSeasons.encode(leagues_seasons)
  end

  def render("index.json", %{leagues_seasons: leagues_seasons}) do
    %{data: render_many(leagues_seasons, LeaguesSeasonView, "leagues_season.json")}
  end

  def render("leagues_season.json", %{leagues_season: leagues_season}) do
    %{
      league: leagues_season.league,
      season: leagues_season.season
    }
  end
end

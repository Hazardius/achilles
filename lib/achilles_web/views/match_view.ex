defmodule AchillesWeb.MatchView do
  use AchillesWeb, :view
  alias AchillesWeb.MatchView

  def render("index.proto", %{matches: matches}) do
    Achilles.Proto.Matches.encode(matches)
  end

  def render("index.json", %{matches: matches}) do
    %{data: render_many(matches, MatchView, "match.json")}
  end

  def render("match.json", %{match: match}) do
    %{
      id: match.id,
      league: match.league,
      season: match.season,
      date: match.date,
      home_team: match.home_team,
      away_team: match.away_team,
      fthg: match.fthg,
      ftag: match.ftag,
      ftr: match.ftr,
      hthg: match.hthg,
      htag: match.htag,
      htr: match.htr
    }
  end
end

defmodule AchillesWeb.MatchController do
  use AchillesWeb, :controller

  alias Achilles.Matches

  action_fallback(AchillesWeb.FallbackController)

  def leagues_seasons_index_proto(conn, _params) do
    leagues_seasons =
      Achilles.Proto.LeaguesSeasons.new(
        leagues_season: Enum.map(Matches.list_leagues_seasons(), &Matches.to_pb_msg_ls/1)
      )

    conn
    |> put_view(AchillesWeb.LeaguesSeasonView)
    |> render("index.proto", leagues_seasons: leagues_seasons)
  end

  def leagues_seasons_matches_proto(conn, broken_params) do
    binary = List.first(Map.keys(broken_params))

    pls = Achilles.Proto.LeaguesSeason.decode(binary)

    matches =
      Matches.to_pb_msg_m(Matches.list_matches_for_leagues_seasons!(pls.league, pls.season))

    render(conn, "index.proto", matches: matches)
  end

  def leagues_seasons_index(conn, _params) do
    leagues_seasons = Matches.list_leagues_seasons()

    conn
    |> put_view(AchillesWeb.LeaguesSeasonView)
    |> render("index.json", leagues_seasons: leagues_seasons)
  end

  def leagues_seasons_matches(conn, %{"league" => league, "season" => season}) do
    matches = Matches.list_matches_for_leagues_seasons!(league, season)
    render(conn, "index.json", matches: matches)
  end
end

# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :achilles,
  ecto_repos: [Achilles.Repo]

# Configures the endpoint
config :achilles, AchillesWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "OJ+UkwpMTPFvWKLDnbn9AYLlIR03UyiFDobEqpTgzu0HyzP7nWfLfgPg8NKS+DP3",
  render_errors: [view: AchillesWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Achilles.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Configures additional mime type for protobuf
config :mime, :types, %{
  "application/x-protobuf" => ["proto"]
}

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

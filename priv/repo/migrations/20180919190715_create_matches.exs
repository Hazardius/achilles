defmodule Achilles.Repo.Migrations.CreateMatches do
  use Ecto.Migration

  def change do
    create table(:matches) do
      add(:league, :string, default: "Unknown", null: false)
      add(:season, :string, default: "Unknown", null: false)
      add(:date, :date)
      add(:home_team, :string, default: "Unknown", null: false)
      add(:away_team, :string, default: "Unknown", null: false)
      add(:fthg, :integer, default: 0)
      add(:ftag, :integer, default: 0)
      add(:ftr, :string, default: "D", null: false)
      add(:hthg, :integer, default: 0)
      add(:htag, :integer, default: 0)
      add(:htr, :string, default: "D", null: false)

      timestamps()
    end

    create(index(:matches, [:league, :season], name: :leagues_seasons))
  end
end

# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Achilles.Repo.insert!(%Achilles.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

# 2018-09-20 marcin
# Current code is based on an example from:
#   http://wsmoak.net/2016/02/16/phoenix-ecto-seeds-csv.html
# Original was shared on a CC BY-NC 3.0 license:
#   https://creativecommons.org/licenses/by-nc/3.0/
# Changes made to match the data.csv file and data contained in it.

alias Achilles.Matches.Match
alias Achilles.Repo

defmodule Achilles.Seeds do
  def store_it(row) do
    IO.inspect(row)
    row = fix_date(row)
    row = fix_season(row)
    changeset = Match.changeset(%Match{}, row)
    Repo.insert!(changeset)
  end

  def fix_date(%{date: <<d1, d0, "/", m1, m0, "/", y1, y0>>} = row) do
    date = <<"20", y1, y0, "-", m1, m0, "-", d1, d0>>
    Map.update!(row, :date, fn _ -> date end)
  end

  def fix_season(%{season: <<"20", f1, f0, s1, s0>>} = row) do
    season = <<"20", f1, f0, "-20", s1, s0>>
    Map.update!(row, :season, fn _ -> season end)
  end
end

File.stream!("./priv/repo/seeds/data.csv")
|> Stream.drop(1)
|> CSV.decode!(
  headers: [
    :id,
    :league,
    :season,
    :date,
    :home_team,
    :away_team,
    :fthg,
    :ftag,
    :ftr,
    :hthg,
    :htag,
    :htr
  ]
)
|> Enum.each(&Achilles.Seeds.store_it/1)

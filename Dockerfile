FROM bitwalker/alpine-elixir-phoenix

# Create and set home directory
ENV HOME /app
RUN mkdir -p /app/build

# Configure required environment
ARG VERSION=0.0.1
ENV MIX_ENV prod

# Set and expose PORT environmental variable
ENV PORT ${PORT:-4000}

WORKDIR /app/build

COPY . /app/build

RUN rm -rf _build deps rel && \
  mix deps.get && \
  mix compile && \
  mix release.init && \
  mix release
RUN mv _build/prod/rel/achilles /app
RUN cd /app

WORKDIR $HOME

ENV PORT ${PORT:-4000}
EXPOSE 4000

# CMD cd /app/build; mix ecto.reset
CMD cd /app/build; mix phx.server

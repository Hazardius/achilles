# Achilles

Small web application providing HTTP API(JSON and Protocol Buffers) access to
its internal database of soccer matches results.

## API documentation

API of Achilles is described in [API.md file](API.md).

## Running dev locally

To build and run this project you need to ensure that you have `elixir` installed.
After that you can just follow those steps:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phx.server`

Now you can use the [`localhost:4000/api`](http://localhost:4000/api) in your
browser to send requests to the JSON API.

More on using the Protocol Buffers API can be found in [proto README.md](proto/README.md).

You can also create the release procution version locally:

    mix release.init
    MIX_ENV=prod mix release

## Generation of Protocol Buffers for Phoenix

To know more about generation of Protocol Buffers - read [proto README.md](proto/README.md).

## Running the dockerized version of Achilles

You need `docker`, `docker-machine` and `docker-compose` to run Achilles from Docker.

On some systems it may be needed to restart the `docker-machine` after boot:

    docker-machine restart
    eval $(docker-machine env)

During the first run we need to create and migrate the PostgreSQL DB.
To do that - please, go to the [Dockerfile](Dockerfile) and change the comments
on the two last lines which should result in them being similar to those:

    CMD cd /app/build; mix ecto.reset
    # CMD cd /app/build; mix phx.server

### Note about running the single web instance and no HAProxy

To run just basic grid(1 web + 1 db), use the `docker-compose-small.yml` file
when running docker-compose, eg.:

    docker-compose -f docker-compose-small.yml up

### Running whole grid

After that we can let the Docker build Achilles image:

    docker-compose -f docker-compose-small.yml build

Next we need to run the Docker in this state once to let it seed the DB:

    docker-compose -f docker-compose-small.yml up

After all the data are loaded to the DB from the CSV file we can stop
the execution with `Ctrl+C`(used once for gracefull exit and used twice for
forced exit).

Because of the time it may take the PostgreSQL to start - we might to run
the second command twice: first time for PostgreSQL initialization and then for
running migrations for Achilles.

We use small grid for those commands to avoid running migrations more than once.

We change back the last two lines in the [Dockerfile](Dockerfile) to
the original state and can rebuild and run the Achilles:

    docker-compose build
    docker-compose up

Be aware that the URL responding for the API requests will most probably be
different than the localhost.
Check the current IP of the used docker-machine:

    docker-machine ip default

Then you can access the API on the `[GIVEN_IP]:4000/api`.
